#ifndef ARTURO_H_
#define ARTURO_H_

#include <iostream>
#include <cassert>
#include <assert.h>
using namespace std;

/*
 * IMPORTANTE!
 * Se puede asumir que el tipo T tiene constructor por copia y operator == y operator <<
 * No se puede asumir que el tipo T tenga operator =
 */
template<typename T>
class Arturo {

  public:

  /*
   * Crea una mesa redonda nueva sin caballeros.
   */
  Arturo();

  /*
   * Una vez copiados, ambos Arturos deben ser independientes,
   * es decir, cuando se borre uno no debe borrar la otra.
   */
  Arturo(const Arturo<T>& otro);

  /*
   * Acordarse de liberar toda la memoria!
   */
  ~Arturo();

  /*
  * Arturo se sienta siempre arturo.
  *
  * PRE: La mesa esta vacía.
  * POST: Arturo está hablando y es el único en la mesa.
  */
  void sentarArturo(const T& a);

  /*
   * Agrega un nuevo caballero a la mesa. El nuevo caballero se sentará
   * a la derecha de Arturo. No se pueden agregar caballeros repetidos.
   *
   * PRE: Arturo esta sentado en la mesa y el nuevo caballero c no está sentado aún
   * POST: El caballero c se sienta a la derecha de Arturo.
   */
  void incorporarCaballero(const T& c);

  /*
   * Elimina de la mesa al caballero pasado por parámetro. En caso de borrar al
   * caballeroActual, pasará a ser el actual que esté sentado a su derecha.
   * Si se expulsa al caballero interrumpido por Arturo, Arturo sigue hablando
   * como si nunca hubiera interrumpido, es decir, el próximo es el de la
   * derecha de Arturo y el anterior es el de la izquierda de Arturo.
   *
   * IMPORTANTE: Sólo se puede borrar a Arturo, si es el único sentado en la mesa.
   *
     * PRE: c puede ser Arturo sólo si la mesa tiene tamaño 1
     * POST: El caballero c no está más sentado en la mesa.
   */
  void expulsarCaballero(const T& c);

    /*
   * Devuelve al caballero que está hablando en este momento.
   *
   * PRE: Hay caballeros sentados en la mesa.
   */
  const T& caballeroActual() const;

  /*
   * Cambia el turno del caballero actual, al de su derecha. Si Arturo
   * interrumpió, el turno pasa al de la derecha del interrumpido.
   * El resultado no tiene interrumpido.
   *
   * PRE: Hay caballeros sentados en la mesa.
   */
  void proximoCaballero();

  /*
   * Cambia el turno del caballero actual, al de su izquierda. Si Arturo
   * interrumpió, el turno pasa al de la izquierda del interrumpido.
   * Esta función puede ser llamada varias veces seguidas.
   * El resultado no tiene interrumpido.
   *
   * PRE: Hay caballeros sentados en la mesa.
   */
  void caballeroAnterior();

  /*
  * Arturo puede elegir que es su turno de hablar en cualquier momento e
  * interrumpir al que está hablando. Arturo termina de hablar por interrupción
  * cuando se pasa al proximoCaballero() o al caballeroAnterior().
  *
  * PRE: Si Arturo está hablando, no se puede interumpir a sí mismo.
  */
  void hablaArturo();

    /*
   * Indica si Arturo está sentado en la mesa.
   */
  bool arturoPresente() const;

  /*
  * Arturo quiere separar un conflicto y por lo tanto cambia su posición y
  * se sienta a la derecha del caballero pasado como parámetro.
  * Tanto como el que está hablando como el interrumpido siguen siendo los mismos.
  * Ejemplos:
  *  cambiarDeLugar(c1) de la mesa: [Arturo(c0), c1, c2] deja la mesa: [Arturo(c0), c2, c1]
  *  cambiarDeLugar(c1) de la mesa: [c1, c2, Arturo(c0)] deja la mesa: [c1, Arturo(c0), c2]
  *  cambiarDeLugar(c2) de la mesa: [c1, c2, Arturo(c0), c3] deja la mesa: [c1, c2, Arturo(c0), c3]
  *
  * PRE: La mesa tiene al menos tamaño 3
  * POST: Arturo está sentado a la derecha de c
  */
  void cambioDeLugar(const T& c);

  /*
   * Dice si la mesa tiene o no caballeros sentados.
   */
  bool esVacia() const;

  /*
   * Devuelve la cantidad de caballeros en la mesa.
   */
  int tamanio() const;

  /*
   * Devuelve true si las mesas son iguales.
   * Dos mesas son iguales cuando todos sus caballeros son iguales,
   * están sentados en las mismas posiciones, y además tanto Arturo,
   * como el que está hablando, como el interrumpido (si hubiera) son iguales.
   */
  bool operator==(const Arturo<T>& otro) const;

  /*
   * Debe mostrar la mesa por el ostream os (y retornar el mismo).
   * Mesa vacia: []
   * Mesa con caballero c0 como Arturo: [ARTURO(c0)]
   * Mesa con 2 caballeros (Arturo está hablando): [ARTURO(c0), c1]
   * Mesa con 3 caballeros (Arturo está hablando): [ARTURO(c0), c1, c2]
   * Mesa con 3 caballeros (c1 está hablando): [c1, c2, ARTURO(c0)]
   * Mesa con 3 caballeros (c1 fue interrumpido): [ARTURO(c0),*c1,c2]
   */
  ostream& mostrarArturo(ostream& os) const;


  private:
  /*
   * No se puede modificar esta funcion.
   */
  Arturo<T>& operator=(const Arturo<T>& otra) {
    assert(false);
    return *this;
  }

  /*
   * Aca va la implementación del nodo.
   */
  struct Nodo {
    const T* dato;
    Nodo* sig;
    Nodo* ant;
  };

  int length;
  Nodo* actual;
  Nodo* arturo;
  Nodo* interrumpido;
};

template<class T>
ostream& operator<<(ostream& out, const Arturo<T>& a) {
  return a.mostrarArturo(out);
}

// Implementación a hacer por los alumnos.
template<class T>
Arturo<T>::Arturo() {
  length = 0;
  actual = NULL;
  arturo = NULL;
}

template<class T>
Arturo<T>::Arturo(const Arturo<T>& otro) {
  length = 0;
  actual = NULL;
  arturo = NULL;
  interrumpido = NULL;
  if(!otro.esVacia()) {
    length++;
    arturo = new Nodo();
    arturo -> dato = new T(*(otro.arturo -> dato));
    arturo -> sig = arturo;
    arturo -> ant = arturo;
    if(otro.actual == otro.arturo) { actual = arturo; }

    Nodo* anterior = arturo;
    Nodo* caballero = otro.arturo -> sig;
    while(caballero != otro.arturo) {
      Nodo* n = new Nodo();
      n -> dato = new T(*(caballero -> dato));
      n -> ant = anterior;
      anterior -> sig = n;
      n -> sig = arturo;

      if(otro.actual == caballero) { actual = n; }

      anterior = n;
      caballero = caballero -> sig;
      length++;
    }

    interrumpido = otro.interrumpido;
  }
}

template<class T>
Arturo<T>::~Arturo() {
  if(!esVacia()) {
    Nodo* caballero = arturo -> sig;
    Nodo* sCaballero = NULL;
    while(caballero != arturo) {
      sCaballero = caballero -> sig;
      expulsarCaballero(*(caballero -> dato));
      caballero = sCaballero;
    }
    expulsarCaballero(*(arturo -> dato));
  }
}

template<class T>
void Arturo<T>::sentarArturo(const T& a) {
  //Precondicion: La mesa esta vacia
  assert(length == 0);
  //Seteo length en 1, y creo el nodo n
  Nodo* n = new Nodo();
  n -> dato = new T(a);
  n -> sig = n;
  n -> ant = n;
  //N es arturo y el actual
  actual = n;
  arturo = n;
  interrumpido = NULL;
  length = 1;
}

template<class T>
void Arturo<T>::incorporarCaballero(const T& c) {
  //Precondicion: Arturo pertence a la mesa y el caballero no existe
  assert(arturoPresente());
  //Aumento en 1 length y creo en nuevo nodo
  Nodo* n = new Nodo();
  n -> dato = new T(c);
  n -> ant = arturo;
  n -> sig = arturo -> sig;
  //Cambiar sig arturo
  (arturo -> sig) -> ant = n;
  //Cambiar arturo
  arturo -> sig = n;
  length++;
}

template<class T>
void Arturo<T>::expulsarCaballero(const T& c) {
  //Precondicion: Solo puedo borrar a arturo si es el unico
  if(length == 1 && *(arturo -> dato) == c) {
    delete arturo -> dato;
    delete arturo;
    length = 0;
    interrumpido = NULL;
    actual = NULL;
    arturo = NULL;
    return;
  }

  //Encuentra al caballero
  Nodo* caballero = arturo -> sig;
  Nodo* sCaballero = NULL;
  //Recorro la mesa hasta encontrarlo y lo elmino, si no no hago nada
  while(caballero != arturo) {
    if(*(caballero -> dato) == c) {
      (caballero -> sig) -> ant = caballero -> ant;
      (caballero -> ant) -> sig = caballero -> sig;
      if(actual == caballero) { actual = caballero -> sig; }
      if(interrumpido == caballero) { actual = arturo; interrumpido = NULL; }
      length--;
      delete caballero -> dato;
      delete caballero;
      return;
    }
    sCaballero = caballero -> sig;
    caballero = sCaballero;
  }
}

template<class T>
const T& Arturo<T>::caballeroActual() const {
  return *(actual -> dato);
}

template<class T>
void Arturo<T>::proximoCaballero() {
  //Si hay alguien interrumpido darle la palabra al proximo del interrumpido
  if(interrumpido != NULL) {
    actual = interrumpido -> sig;
    interrumpido = NULL;
  }
  //Sino darle la palabra al siguiente
  else {
    actual = actual -> sig;
  }
}

template<class T>
void Arturo<T>::caballeroAnterior() {
  //Si hay alguien interrumpido darle la palabra al proximo del interrumpido
  if(interrumpido != NULL) {
    actual = interrumpido -> ant;
    interrumpido = NULL;
  }
  //Sino darle la palabra al siguiente
  else {
    actual = actual -> ant;
  }
}

template<class T>
void Arturo<T>::hablaArturo() {
  //Precondicion: Arturo no se puede interrumpir a si mismo
  assert(actual != arturo);
  interrumpido = actual;
  actual = arturo;
}

template<class T>
bool Arturo<T>::arturoPresente() const {
  return arturo != NULL;
}

template<class T>
void Arturo<T>::cambioDeLugar(const T& c) {
  //Precondicion: La mesa tiene al menos 3 caballeros
  assert(length >= 3);

  Nodo* caballero;
  if(*(actual -> dato) == c) { caballero = actual; }
  else {
    caballero = actual -> sig;
    while(caballero != actual) {
      if(*(caballero -> dato) == c) { }
      caballero = caballero -> sig;
    }
  }

  Nodo* sCaballero = caballero -> sig;
  if(caballero != arturo) {
    caballero -> sig = arturo;
    sCaballero -> ant = arturo;
    (arturo -> sig) -> ant = arturo -> ant;
    (arturo -> ant) -> sig = arturo -> sig;
    arturo -> sig = sCaballero;
    arturo -> ant = caballero;
  }
}

template<class T>
bool Arturo<T>::esVacia() const {
  return length == 0;
}

template<class T>
int Arturo<T>::tamanio() const {
  return length;
}

template<class T>
bool Arturo<T>::operator==(const Arturo<T>& otro) const {
  if(esVacia() && otro.esVacia()) { return true; }
  else if(esVacia() || otro.esVacia()) { return false; }
  else {
    //Si los arturos son diferentes o el actual o la longitud es diferente return false
    if(*(arturo -> dato) != *(otro.arturo -> dato) || length != otro.length || *(actual -> dato) != *(otro.actual -> dato))
      { return false; }
    //Chequeo que los interrumpido sean iguales
    if((interrumpido != NULL && otro.interrumpido != NULL && *(interrumpido -> dato) != *(otro.interrumpido -> dato)) || (interrumpido != NULL && otro.interrumpido == NULL) || (interrumpido == NULL && otro.interrumpido != NULL))  { return false; }
    
    //Sino checkear los caballeros uno por uno hasta llegar a arturo nuevamente
    Nodo* caballero = arturo -> sig;
    Nodo* oCaballero = otro.arturo -> sig;
    while(caballero != arturo) {
      if(*(caballero -> dato) != *(oCaballero -> dato)) { return false; }
      caballero = caballero -> sig;
      oCaballero = oCaballero -> sig;
    }
    return true;
  }
}

template<class T>
ostream& Arturo<T>::mostrarArturo(ostream& os) const {
  os << "[";

  if(!esVacia()) {
    if(length == 1) { os << "ARTURO(" << *(arturo -> dato) << ")"; }
    else {
      if(actual == arturo) { os << "ARTURO(" << *(actual -> dato) << "), "; }
      else if(actual == interrumpido) { os << "*" << *(actual -> dato) << ", "; }
      else { os << *(actual -> dato) << ", "; }

      Nodo* caballero = actual -> sig;
      while(caballero != actual) {
        if(caballero == arturo) { os << "ARTURO(" << *(caballero -> dato) << ")"; }
        else if(caballero == interrumpido) { os << "*" << *(caballero -> dato); }
        else { os << *(caballero -> dato); }

        caballero = caballero -> sig;
        
        if(caballero != actual) { os << ", "; }
      }
    }
  }

  os << "]";
  
  return os;
}

#endif //ARTURO_H_
